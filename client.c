/*
*Client.c
*
*
*/

#ifndef client_c
#define client_c

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <semaphore.h>
#include <errno.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/msg.h>

#include "client.h"
#include "message.h"

FILE *in;

int getData();

int main(int argc)
{
	client_t client;
	client.id = argc;
	msgBuf msg1, msg2;
	key_t msgQueKey;
	int queID;
	int msgStatus;
	
	msgQueKey = BASE_MAILBOX_NAME;
	queID = msgget(msgQueKey, IPC_CREAT | 0600);
	if ( queID < 0)
	{
		printf("Failed to create mailbox %x. Error Code = %d\n"
			,msgQueKey,errno);
		exit(-2);
	}
	msg1.mtype = 1;
	msg1.info.sender = getpid();
	msg1.info.client = client.id;
	
	msgStatus = msgsnd (queID, &msg1, MSG_INFO_SIZE, 0);
	if (msgStatus < 0)
	{
		printf("Failed to send message to banker on id %d. Error code =%d\n"
			,queID, errno);
		exit(-2);
	}
	printf("\nClient is now waiting for a reply from banker...\n");
	msgStatus = msgrcv(queID,&msg2,MSG_INFO_SIZE, 2, 0);
	if (msgStatus < 0 )
	{
		printf("Failed to receive message from Banker process on queID %d. 
			Error code = %d\n",queId, errno);
		exit(-2);
	}
		
}


int getData()
{
	in = fopen("initial.data","r");
	
	//fscanf(in, "%d",  &
}
#endif
